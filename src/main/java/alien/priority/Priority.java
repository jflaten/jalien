package alien.priority;


import lazyj.DBFunctions;

import java.io.Serializable;

public class Priority implements Comparable<Priority>, Serializable {

    public Priority() {
    }

    public Priority(final DBFunctions db) {
        this.userId = db.geti("userId");
        this.priority = db.getf("priority");
        this.maxParallelJobs = db.geti("maxparallelJobs");
        this.unfinishedJobsLast24h = db.geti("unfinishedJobsLast24h");
        this.userload = db.getf("userload");
        this.nominalParallelJobs = db.geti("nominalparallelJobs");
        this.maxTotalRunningTime = db.getl("maxTotalRunningTime");
        this.maxUnfinishedJobs = db.geti("maxUnfinishedJobs");
        this.computedPriority = db.getf("computedPriority");
        this.maxTotalCpuCost = db.getf("maxTotalCpuCost");
        this.totalRunningTimeLast24h = db.getl("totalRunningTimeLast24h");
        this.waiting = db.geti("waiting");
        this.running = db.geti("running");
        this.totalCpuCostLast24h = db.getf("totalCpuCostLast24h");
    }

    /**
     * User id
     */
    private int userId;

    /**
     * User baseline priority
     */
    private float priority;

    /**
     * Maximum number of cpu cores that can be utilized simulataneously by a user
     */
    private int maxParallelJobs;

    /**
     *
     */
    private int unfinishedJobsLast24h;

    /**
     * Current user load = runningJobs / maxParallelJobs
     */
    private float userload;

    /**
     *
     */
    private int nominalParallelJobs;

    /**
     *
     */
    private long maxTotalRunningTime;

    /**
     *
     */
    private int maxUnfinishedJobs;

    /**
     * computed priority determines which user gets priority to run a job.
     */
    private float computedPriority;

    /**
     * ??
     */
    private float maxTotalCpuCost;

    /**
     * Total running time of all jobs of this user in the last 24 hours
     */
    private long totalRunningTimeLast24h;

    /**
     * Number of waiting jobs
     */
    private int waiting;

    /**
     * Number of running jobs
     */
    private int running;

    /**
     * Cpu cost the last 24 hours
     */
    private float totalCpuCostLast24h;


    @Override
    public String toString() {
        return "Priority{" +
                "userId=" + userId +
                ", priority=" + priority +
                ", maxParallelJobs=" + maxParallelJobs +
                ", unfinishedJobsLast24h=" + unfinishedJobsLast24h +
                ", userload=" + userload +
                ", nominalParallelJobs=" + nominalParallelJobs +
                ", maxTotalRunningTime=" + maxTotalRunningTime +
                ", maxUnfinishedJobs=" + maxUnfinishedJobs +
                ", computedPriority=" + computedPriority +
                ", maxTotalCpuCost=" + maxTotalCpuCost +
                ", totalRunningTimeLast24h=" + totalRunningTimeLast24h +
                ", waiting=" + waiting +
                ", running=" + running +
                ", totalCpuCostLast24h=" + totalCpuCostLast24h +
                '}';
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public float getPriority() {
        return priority;
    }

    public void setPriority(float priority) {
        this.priority = priority;
    }

    public int getMaxParallelJobs() {
        return maxParallelJobs;
    }

    public void setMaxParallelJobs(int maxParallelJobs) {
        this.maxParallelJobs = maxParallelJobs;
    }

    public int getUnfinishedJobsLast24h() {
        return unfinishedJobsLast24h;
    }

    public void setUnfinishedJobsLast24h(int unfinishedJobsLast24h) {
        this.unfinishedJobsLast24h = unfinishedJobsLast24h;
    }

    public float getUserload() {
        return userload;
    }

    public void setUserload(float userload) {
        this.userload = userload;
    }

    public int getNominalParallelJobs() {
        return nominalParallelJobs;
    }

    public void setNominalParallelJobs(int nominalParallelJobs) {
        this.nominalParallelJobs = nominalParallelJobs;
    }

    public long getMaxTotalRunningTime() {
        return maxTotalRunningTime;
    }

    public void setMaxTotalRunningTime(long maxTotalRunningTime) {
        this.maxTotalRunningTime = maxTotalRunningTime;
    }

    public int getMaxUnfinishedJobs() {
        return maxUnfinishedJobs;
    }

    public void setMaxUnfinishedJobs(int maxUnfinishedJobs) {
        this.maxUnfinishedJobs = maxUnfinishedJobs;
    }

    public float getComputedPriority() {
        return computedPriority;
    }

    public void setComputedPriority(float computedPriority) {
        this.computedPriority = computedPriority;
    }

    public float getMaxTotalCpuCost() {
        return maxTotalCpuCost;
    }

    public void setMaxTotalCpuCost(float maxTotalCpuCost) {
        this.maxTotalCpuCost = maxTotalCpuCost;
    }

    public long getTotalRunningTimeLast24h() {
        return totalRunningTimeLast24h;
    }

    public void setTotalRunningTimeLast24h(long totalRunningTimeLast24h) {
        this.totalRunningTimeLast24h = totalRunningTimeLast24h;
    }

    public int getWaiting() {
        return waiting;
    }

    public void setWaiting(int waiting) {
        this.waiting = waiting;
    }

    public int getRunning() {
        return running;
    }

    public void setRunning(int running) {
        this.running = running;
    }

    public float getTotalCpuCostLast24h() {
        return totalCpuCostLast24h;
    }

    public void setTotalCpuCostLast24h(float totalCpuCostLast24h) {
        this.totalCpuCostLast24h = totalCpuCostLast24h;
    }

    @Override
    public int compareTo(Priority o) {
        return 0;
    }
}
