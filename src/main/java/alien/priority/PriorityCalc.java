package alien.priority;

import alien.config.ConfigUtils;
import alien.optimizers.priority.CalculateComputedPriority;
import alien.optimizers.priority.PriorityRapidUpdater;
import alien.optimizers.priority.PriorityReconciliationService;
import alien.taskQueue.TaskQueueUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author jaFlaten
 * @since 2023-11-22
 */
public class PriorityCalc {
    static final Logger logger = ConfigUtils.getLogger(PriorityCalc.class.getCanonicalName());

    public static void main(String[] args) {
        //updatePriorityDev();

        //get priority from the database
        //getPriorities();

//        runAndPrintJobsUsingOriginalFormula();
        //runAndPrintJobsUsingOriginalFormulaByAddingMultipleCpuCores();

        // Run reconciliation service
        PriorityReconciliationService s = new PriorityReconciliationService();
        s.start();


        // run priority updater service
        PriorityRapidUpdater u = new PriorityRapidUpdater();
        u.start();


        //run computedpriority calculation service
        CalculateComputedPriority c = new CalculateComputedPriority();
        c.start();

        // formula with cpu core cost included.
        //price = (execution time * number of cores) * number of jobs ?

    }

    private static void updatePriorityDev() {
        List<Priority> priorities = getPriorities();
        if (priorities != null) {
            for (Priority p : priorities) {
                updateComputedPriority(p);
            }

            TaskQueueUtils.updatePriorityPerUser(priorities);
        }
    }

    // to create database entries
    private static void insertPriorityDev() {
        List<Priority> priorities = getPriorities();
        if (priorities != null) {
            TaskQueueUtils.insertPriorityInDev(priorities);
        }
    }

    private static List<Priority> getPriorities() {
        List<Priority> priority = TaskQueueUtils.getPriority();

        if (priority != null) {
            logger.log(Level.INFO, "Elements in Priority list: " + priority.size());
            priority = findComputedPriority(priority);
        }

        return priority;
    }

    private static void runAndPrintJobsUsingOriginalFormula() {
        List<User> users1 = getUsers1();
        List<User> users2 = getUsers2();
        List<User> users3 = getUsers3();

        List<RunningJobs> runningJobs1 = computeAndRunOriginalFormula(users1, 100000);
        List<RunningJobs> runningJobs2 = computeAndRunOriginalFormula(users2, 10000);
        List<RunningJobs> runningJobs3 = computeAndRunOriginalFormula(users3, 10000);

        try {
            writeToCSV(runningJobs1, "running_jobs1.csv");
            System.out.println("Data written to running_jobs1.csv");

            writeToCSV(runningJobs2, "running_jobs2.csv");
            System.out.println("Data written to running_jobs2.csv");

            writeToCSV(runningJobs3, "running_jobs3.csv");
            System.out.println("Data written to running_jobs3.csv");
        } catch (IOException e) {
            System.out.println("Error writing to CSV: " + e.getMessage());
        }
    }

    //rewrite to utilize Priority object

    public static List<Priority> findComputedPriority(List<Priority> priorities) {
        for (Priority p : priorities) {
            // user runtime
//            double computedPriority = calculateComputedPriority(p.getPriority(), p.getRunning(), p.getMaxParallelJobs(), p.getTotalRunningTimeLast24h());

            // hardcoded runtime
            int activeCores = p.getRunning();
            int runtime = activeCores * 25;
            double computedPriority = refactoredCalculateComputedPriority4(p.getPriority(), activeCores, p.getMaxParallelJobs(), runtime);

            p.setComputedPriority((float) (computedPriority));
            //logger.log(Level.INFO, "GET: " + p.toString());
        }

        return priorities;
    }

    private static void runAndPrintJobsUsingOriginalFormulaByAddingMultipleCpuCores() {
        List<User> users1 = getUsers1();
        List<User> users2 = getUsers2();
        List<User> users3 = getUsers3();
        List<User> users4 = getUsers4();

        List<RunningJobs> runningJobs1 = computeAndRunOriginalFormulaByAddingMultipleCpuCores(users1, 100000);
        List<RunningJobs> runningJobs2 = computeAndRunOriginalFormulaByAddingMultipleCpuCores(users2, 10000);
        List<RunningJobs> runningJobs3 = computeAndRunOriginalFormulaByAddingMultipleCpuCores(users3, 10000);
        List<RunningJobs> runningJobs4 = computeAndRunOriginalFormulaByAddingMultipleCpuCores(users4, 10000);

        try {
            writeToCSV(runningJobs1, "running_multi-core_1.csv");
            System.out.println("Data written to running_multi-core_1.csv");

            writeToCSV(runningJobs2, "running_multi-core_2.csv");
            System.out.println("Data written to running_multi-core_2.csv");

            writeToCSV(runningJobs3, "running_multi-core_3.csv");
            System.out.println("Data written to running_multi-core_3.csv");

            writeToCSV(runningJobs4, "running_multi-core_4.csv");
            System.out.println("Data written to running_multi-core_4.csv");
        } catch (IOException e) {
            System.out.println("Error writing to CSV: " + e.getMessage());
        }
    }


    private static List<RunningJobs> computeAndRunOriginalFormula(List<User> users, int maxJobs) {
        return computeCpuCoresInUse(users, maxJobs, 1, new ArrayList<>());
    }

    private static List<RunningJobs> computeAndRunOriginalFormulaByAddingMultipleCpuCores(List<User> users, int maxCpuCores) {
        int seed = 12345;
        List<Integer> cores = getRepeatableRandomNumberOfCpuCores(seed, maxCpuCores);
        return computeCpuCoresInUse(users, maxCpuCores, 0, cores);
    }

    public static List<RunningJobs> computeCpuCoresInUse(List<User> users, int maxCpuCoresInUse, int cpuCoresForAJob, List<Integer> cores) {
        int counterTotalCpuCoresInUse = 0; // job * cpu core usage for that job

        List<RunningJobs> runningJobs = new ArrayList<>();

        while (counterTotalCpuCoresInUse < maxCpuCoresInUse) {
            List<Map<Integer, Double>> userPriorities = findUserPriorities(users);

            Optional<Map.Entry<Integer, Double>> highestComputedPriorityUser = userPriorities.stream()
                    .flatMap(m -> m.entrySet().stream())
                    .max(Map.Entry.comparingByValue());

            if (highestComputedPriorityUser.isPresent()) {
                Integer userId = highestComputedPriorityUser.get().getKey();
                Optional<User> optUser = users.stream()
                        .filter(p -> p.getId() == userId)
                        .findFirst();

                if (highestComputedPriorityUser.get().getValue() == 0.0)
                    break;

                if (highestComputedPriorityUser.get().getValue() == 1.0)
                    break;

                if (optUser.isPresent()) {
                    User user = optUser.get();
                    if (user.getRunningJobs() < user.getMaxParallelJobs()) {
                        if (cpuCoresForAJob == 1) {
                            user.setRunningJobs(user.getRunningJobs() + cpuCoresForAJob);
                            counterTotalCpuCoresInUse += cpuCoresForAJob;
                        } else {
                            int coresForAJob = cores.get(counterTotalCpuCoresInUse);
                            user.setRunningJobs(user.getRunningJobs() + coresForAJob);
                            counterTotalCpuCoresInUse += coresForAJob;
                        }
                        users.set(userId, user);
                    }
                    runningJobs.add(
                            new RunningJobs(
                                    counterTotalCpuCoresInUse, user.getId(), user.getRunningJobs(), highestComputedPriorityUser.get().getValue()));

                    System.out.println("User " + user.getId() + " has priority " + highestComputedPriorityUser.get().getValue() + " and is running " + user.getRunningJobs() + " jobs");
                    System.out.println("counter: " + counterTotalCpuCoresInUse + "  max: " + maxCpuCoresInUse);
                }
            } else {
                System.out.println("No user with highest priority found");
            }

        }
        System.out.println("Total CPU cores in use to run jobs: " + counterTotalCpuCoresInUse);

        return runningJobs;
    }

    public static List<Map<Integer, Double>> findUserPriorities(List<User> users) {
        List<Map<Integer, Double>> userPriorities = new ArrayList<>();
        for (User u : users) {
            // comment in for original formula
            //double computedPriority = refactoredOriginalCalculateComputedPriority(u.getDefaultPriority(), u.getRunningJobs(), u.getMaxParallelJobs());

            // comment in for updated formula
            int activeCores = u.getRunningJobs();
            int runtime = activeCores * 25;
            double computedPriority = refactoredCalculateComputedPriority4(u.getDefaultPriority(), activeCores, u.getMaxParallelJobs(), runtime);

            userPriorities.add(Map.of(u.getId(), computedPriority));
        }
        return userPriorities;
    }

    public static double originalCalculateComputedPriority(double userPriority, double running, double maxParallelJobs) {
        double userload = running / maxParallelJobs;
        return (running < maxParallelJobs) ?
                ((2 - userload) * userPriority > 0 ?
                        50.0 * (2 - userload) * userPriority :
                        1) :
                1;
    }

    public static double refactoredOriginalCalculateComputedPriority(double userPriority, double cpuCoresInUse, double maxParallelCpuCores) {
        double userload = cpuCoresInUse / maxParallelCpuCores;
        return (cpuCoresInUse < maxParallelCpuCores) ?
                ((2 - userload) * userPriority > 0 ?
                        50.0 * (2 - userload) * userPriority :
                        1) :
                1;
    }

    //Make changes here
    // change: reduce from 50 to 25. Only change seemed to be that the priority is lower, but order remains the same.
    // change: multiplied userPriority by 0.33 to reduce the value of priority, but order remains the same.
    // change: removed 2 - userload, to keep userload,now the result is that the user id 1 is the only that gets to run jobs until the end.
    // change: reintroduced calculation on userload. Now 1.5 - userload. Now the result is similar to before when we had 2 - userload.
    // change: adding * 0.33 on userPriority where it gets multiplied with userload does not really change anything. Any number changes does not change how the formula behaves
    // it only changes the values, the order remains the same. The fairness is still the same, not fair. We need to introduce a new parameter to help us affect the order of the computed priority.
    // change: reverted values to original and added a cost for totalCpuCostLast24h. by taking cores in use and multiplying by an arbitrary number for time (50) and then dividing by userPriority.
    public static double refactoredOriginalCalculateComputedPriority2(double userPriority, double cpuCoresInUse, double maxParallelCpuCores) {
        double dummyRunTime = 25;
        double totalCpuCostLast24H = cpuCoresInUse * dummyRunTime;
        if (cpuCoresInUse == 0) {
            totalCpuCostLast24H = 1;
        }
        double userload = cpuCoresInUse / maxParallelCpuCores;
        return (cpuCoresInUse < maxParallelCpuCores) ?
                ((2.0 - userload) * (userPriority / totalCpuCostLast24H) > 0 ?
                        50.0 * (2 - userload) * (userPriority / totalCpuCostLast24H) :
                        1) :
                1;
    }
    //TotalCpuCostLast24H skal være med den er sum(price * runtimes)
    // totalcpucost or totalruntime can be used in the formula.
    // totalruntime, cpu cores, userload, - db columns
    // update number of waiting jobs and maxunfinished


    public static double refactoredCalculateComputedPriority3(double userPriority, double cpuCoresInUse, double maxParallelCpuCores) {
        double dummyRunTime = 25;

        boolean cpuCoresInUseNonZero = cpuCoresInUse > 0;
        double totalCpuCostLast24H = cpuCoresInUseNonZero ? cpuCoresInUse * dummyRunTime : 1;
        double userLoad = cpuCoresInUse / maxParallelCpuCores;
        // Calculate priority multiplier based on user load and user priority.
        double priorityMultiplier = (2.0 - userLoad) * (userPriority / totalCpuCostLast24H);

        // Determine the computed priority.
        if (cpuCoresInUse < maxParallelCpuCores) {
            // If the multiplier is positive, calculate the final priority. Otherwise, return 1.
            return priorityMultiplier > 0 ? 50.0 * priorityMultiplier : 1;
        } else {
            // If more or equal CPU cores are in use than the maximum allowed, return 1.
            return 1;
        }
    }

    public static double refactoredCalculateComputedPriority4(double defaultPriority, int activeCores, int maxCores, long runtime) {
        if (activeCores < maxCores) {
//            baseRuntime = 26740000800L;
            long maxRuntime = 1_000_000_000_000_00L;
            long maxRuntime2 = 1_000_000_000L;
            double historicalUsage = (double) runtime / maxRuntime2;
            double coreUsageCost = activeCores == 0 ? 1 : activeCores * Math.exp(-historicalUsage);
            double userLoad = (double) activeCores / maxCores;
            double adjustedPriorityFactor = (2.0 - userLoad) * (defaultPriority / coreUsageCost);

            if (adjustedPriorityFactor > 0) {
                return 50.0 * adjustedPriorityFactor;
            } else {
                return 1;
            }
        }
        return 1;
    }


    public static double calculateComputedPriority(double userPriority, double cpuCoresInUse, double maxParallelCpuCores, long runtime) {
        double totalCpuCostLast24H = cpuCoresInUse * runtime;
        logger.log(Level.INFO, "Total CPU cost last 24 hours: " + totalCpuCostLast24H);
        double userload = cpuCoresInUse / maxParallelCpuCores;

        if (cpuCoresInUse == 0) {
            totalCpuCostLast24H = 1;
        }

        double userCost = userPriority / totalCpuCostLast24H;

        return (cpuCoresInUse < maxParallelCpuCores) ?
                ((2.0 - userload) * (userCost) > 0 ?
                        50.0 * (2 - userload) * (userCost) :
                        1) :
                1;
    }

    public static void updateComputedPriority(Priority p) {
        int activeCpuCores = p.getRunning();
        int maxCpuCores = p.getMaxParallelJobs();
        long historicalUsage = p.getTotalRunningTimeLast24h() / p.getMaxTotalRunningTime();

        if (activeCpuCores < maxCpuCores) {
            double coreUsageCost = activeCpuCores == 0 ? 1 : (activeCpuCores * Math.exp(-historicalUsage));
            float userLoad = (float) activeCpuCores / maxCpuCores;
            p.setUserload(userLoad);
            double adjustedPriorityFactor = (2.0 - userLoad) * (p.getPriority() / coreUsageCost);

            if (adjustedPriorityFactor > 0) {
                p.setComputedPriority((float) (50.0 * adjustedPriorityFactor));
            } else {
                p.setComputedPriority(1);
            }
        } else {
            p.setComputedPriority(1);
        }
    }

//
//    maxTotalRunningTime     : 100000000000000       (CPU cores * wall time seconds)
//    totalRunningTimeLast24h : 109135614 = 0% of max (resources consumed by jobs completed in the last 24h)
//    maxTotalCpuCost         : 100000000376832       (price * CPU cores * wall time seconds)
//    totalCpuCostLast24h     : 27285899264 = 0.027% of max   (cost of jobs completed in the last 24h)

//    maxUnfinishedJobs       : 1000000               (maximum allowed number of waiting jobs)
//    waiting                 : 782 = 0.078% of max   (currently queued jobs)
//    unfinishedJobsLast24h   : 137                   (jobs waiting since less than 24h or currently active)

    // CPU cost = number of cpu cores * number of jobs
    // CPu cost for a job , total cost er cores
    // job 1: 4 cpu cores  -> cost for den jobben er 4.
    // job 2: 2 cpu cores
    // job 3: 4 cpu cores
    // cost = j1 + j2 + j3 -> 4 + 2 + 4 = 10.

    // user har ein quota med kor masse dei kan køyre, ikkje kor mange jobbar, men med kor mange cores dei kan bruke.
    // bruke frå kvota. så pris blir brukt frå kvota. så sei kvote på 50 og pris er 10 så er det 40 igjen.


    //TODO - inkludere currentlyRunningJobs og maxRunningJobs  - Korleis ? Frå før er jo det Userload         double userload = running / maxParallelJobs;
    //TODO vi kan finne ein current pris gitt alle jobbane ein brukar køyrer
    // Om runningtimeLast24H er veldig høg, så bør det påvirke formelen negativt, altså at prioriteten går ned.
    // kan prioritet bli redusert om dei har betalt så masse at dei har lite igjen av kvoten sin ? då har du kvoten og running timen, kor verdifulle skal dei være
    // Korleis kan kvote og running time spele inn ?
//     - total running time (execution time)
    // - cost
    // - number of cores in use - begynne med ein ny formel  som kun endrer cores
    // korleis kan eg få med 1, 2, 4, 6, 8, 10 og 12 cores? opp til 16.

    public static List<Integer> getRepeatableRandomNumberOfCpuCores(int seed, int iterations) {
        Random random = new Random(seed);
        List<Integer> cores = new ArrayList<>();
        for (int i = 0; i < iterations; i++) {
            int number = (1 + random.nextInt(8));
            if (number == 1 || number == 2 || number == 4 || number == 6 || number == 8)
                cores.add(number);
            else
                cores.add(2 * number);
        }
        return cores;
    }


    public static List<User> getUsers1() {
        return new ArrayList<>(List.of(
                new User(0, 2500.0, 10000, 0),
                new User(1, 5000.0, 200000, 0),
                new User(2, 20000.0, 80000, 0),
                new User(3, 15000.0, 60000, 0),
                new User(4, 15000.0, 85000, 0),
                new User(5, 1000.0, 40000, 0)
        ));
    }

    public static List<User> getUsers2() {
        return new ArrayList<>(List.of(
                new User(0, 12500.0, 10000, 0),
                new User(1, 14000.0, 200000, 0),
                new User(2, 16000.0, 80000, 0),
                new User(3, 15000.0, 60000, 0),
                new User(4, 15500.0, 85000, 0),
                new User(5, 13000.0, 40000, 0)
        ));
    }

    public static List<User> getUsers3() {
        return new ArrayList<>(List.of(
                new User(0, 700.0, 10000, 0),
                new User(1, 1000.0, 10000, 0),
                new User(2, 500.0, 10000, 0),
                new User(3, 1100.0, 10000, 0),
                new User(4, 900.0, 10000, 0),
                new User(5, 800.0, 10000, 0)
        ));
    }


    private static List<User> getUsers4() {
        return new ArrayList<>(List.of(
                new User(0, 1000.0, 5000, 0),
                new User(1, 1000.0, 5000, 0),
                new User(2, 1000.0, 5000, 0),
                new User(3, 10000.0, 1000, 0),
                new User(4, 100000.0, 1000, 0)
        ));
    }

    public static void writeToCSV(List<RunningJobs> runningJobs, String filename) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            // Writing header
            writer.write("Total CPU core usage (Jobs),User ID,User CPU Core usage,Computed Priority start of Job\n");

            // Writing data
            for (RunningJobs job : runningJobs) {
                writer.write(job.getCounterTotalRunningJobs() + "," +
                        job.getUserId() + "," +
                        job.getUserCurrentlyRunningJobs() + "," +
                        job.getComputedPriorityAtJobStart() + "\n");
            }
        }
    }

}
